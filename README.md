# Ibus 2.1

Encode your files in images, decode images to your files

## Download 

Download [Ibus.jar](download/) for every system with a working installation of Java 8 or above.
Alternativle build the jar yourself.

## Use

`java -jar Ibus.jar dataDir --key=X`

starts the program with key X as encryption/Decryption key. The encrypted data should be located in the dataDir.


`--key=Y` encrypts/decrypts the data with aes with the given key. Only the first 16 characters can be utilized since the encryption used is aes-128bit.
Using an incorrect key for decryption may result in errors. Access to your data is lost when you lose your key.
Use `--minsize=x` to force images to have dimensions of at least x times x. Default is minsize=256.
Use `--maxsize=x` to force images to have dimensions of at maximum x times x. Default is maxsize=4000.

Do make Backups of your data before using this program. I'm not responseable for any data you might lose.

## To-do

- correct copy and move behaviour
- add native file system integration

### On ice:
- increase performance: different encoding methods did not yield in remarkable performance improvements
- add multi core support: the algorithm appears to be memory bound, there might be a fix in the future for this but right now multi core support did not yield the expected performance improvements
- add gPhotos automatic up/download

### Cloud problems:
There are currently 2 major cloud storage solutions that offer unlimited photos for free (or a reasonable price):

Google photos offers unlimited storage for photos if they are stored in "high quality" instead of original quality. Pictures for google drive should be at least 256x256 and should not exceed 16 megapixels (4000x4000). There is a google photos api that would allow when used in combination with the method demonstrated in the Ibus project to have unlimited storage, however the api only allows the images to be treated as "original quality" instead of the desired "high quality".

The second solution is amazon photos. Any amazon prime member has access to an unlimited amount of photo storage and there is an api that should do the trick just fine. However it is required to be whitelisted as a developer to have access to this api, which i am not.

In conclusion both providers are in practice not suitable for this project right now. If you know any other cloud storage provider with unlimited storage please let me now. Thanks.


## Further information:

Images have by default a maximal size of 4000x4000. With ARGB colors every image can store at most 64MByte. Larger files are split up in parts.
Folder structures in the original files are not present in the image representation, however they are restored at the decoding step.

The entire file system structure is saved partially encrypted in the indexX.png files. When opening an existing file system make sure the index0.png exists.
After encoding some files you can delete the data files(a.b.c.png or a.b._.png) and still add new files to your indexing structure.
In practice you should consider uploading your data files before deleting them. This enables you to encode your files in batches.

The used format should result in no data loss when using certain image based cloud solutions.

## Update 2.1:
Ibus 2.1 is not compatible with Ibus 2.0 or any previous version since the update introduced an additional timestamp that holds the information about the last modification of the file. This information is crucial for handling changes in a remote location effectively.

