package de.christian.f.brinkmann.ibus.indexing;

public abstract class IndexingEntry {

	private String name;
	private IndexingDir parent;
	private long lastChanged;

	public IndexingEntry(IndexingDir parent, String name, long lastChanged) {
		this.parent = parent;
		this.name = name;
		this.lastChanged = lastChanged;
	}

	public String getName() {
		return name;
	}

	public IndexingDir getParent() {
		return parent;
	}

	public void setParent(IndexingDir parent) {
		this.parent = parent;
	}

	public long getLastChanged() {
		return lastChanged;
	}

	public void setLastChanged(long lastChanged) {
		this.lastChanged = lastChanged;
	}

}
